import nltk
from nltk.chat.util import Chat, reflections
import numpy as np
import random
import string # to process standard python strings
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity
import requests

def chatbot(message):
    _message = message
    
    reflections = {
        "i am"       : "you are",
        "i was"      : "you were",
        "i"          : "you",
        "i'm"        : "you are",
        "i'd"        : "you would",
        "i've"       : "you have",
        "i'll"       : "you will",
        "my"         : "your",
        "you are"    : "I am",
        "you were"   : "I was",
        "you've"     : "I have",
        "you'll"     : "I will",
        "your"       : "my",
        "yours"      : "mine",
        "you"        : "me",
        "me"         : "you"
    }
    
    
    pairs = [
            [
                r"my name is (.*)",
                ["Hello %1, How are you today ?",]
            ],
            [
                r"hi|hey|hello",
                ["Hello", "Hey there",]
            ], 
            [
                r"what is your name ?",
                ["I am a bot created by Analytics Vidhya. you can call me crazy!",]
            ],
            [
                r"how are you ?",
                ["I'm doing goodnHow about You ?",]
            ],
            [
                r"sorry (.*)",
                ["Its alright","Its OK, never mind",]
            ],
            [
                r"I am fine",
                ["Great to hear that, How can I help you?",]
            ],
            [
                r"i'm (.*) doing good",
                ["Nice to hear that","How can I help you?:)",]
            ],
            [
                r"(.*) age?",
                ["I'm a computer program dudenSeriously you are asking me this?",]
            ],
            [
                r"what (.*) want ?",
                ["Make me an offer I can't refuse",]
            ],
            [
                r"(.*) created ?",
                ["Raghav created me using Python's NLTK library ","top secret ;)",]
            ],
            [
                r"(.*) (location|city) ?",
                ['Indore, Madhya Pradesh',]
            ],
            [
                r"how is weather in (.*)?",
                ["Weather in %1 is awesome like always","Too hot man here in %1","Too cold man here in %1","Never even heard about %1"]
            ],
            [
                r"i work in (.*)?",
                ["%1 is an Amazing company, I have heard about it. But they are in huge loss these days.",]
            ],
            [
                r"(.*)raining in (.*)",
                ["No rain since last week here in %2","Damn its raining too much here in %2"]
            ],
            [
                r"how (.*) health(.*)",
                ["I'm a computer program, so I'm always healthy ",]
            ],
            [
                r"(.*) (sports|game) ?",
                ["I'm a very big fan of Football",]
            ],
            [
                r"who (.*) sportsperson ?",
                ["Messy","Ronaldo","Roony"]
            ],
            [
                r"who (.*) (moviestar|actor)?",
                ["Brad Pitt"]
            ],
            [
                r"i am looking for online guides and courses to learn data science, can you suggest?",
                ["Crazy_Tech has many great articles with each step explanation along with code, you can explore"]
            ],
            [
                r"quit",
                ["BBye take care. See you soon :) ","It was nice talking to you. See you soon :)"]
            ],
        ]
    
    
    f = open('./chatbot.txt','r',errors='ignore')

    raw = f.read()

    raw = raw.lower()

    sent_tokens = nltk.sent_tokenize(raw)
    word_tokens = nltk.word_tokenize(raw)
    
    lemmer = nltk.stem.WordNetLemmatizer()

    def LemTokens(tokens):
        return [lemmer.lemmatize(token) for token in tokens]

    remove_punct_dict = dict((ord(punct), None) for punct in string.punctuation)

    def LemNormalize(text):
        return LemTokens(nltk.word_tokenize(text.lower().translate(remove_punct_dict)))

    GREETING_INPUTS = ("hello", "hi", "greetings", "sup", "what's up", "hey",)
    GREETING_RESPONSES = ["hi", "hey", "*nods*", "hi there", "hello", "I am glad! You are talking to me"]
    
    INFORMATIVE_INPUTS = ("offers report", "approved offers", "conversion report")

    def greeting(sentence):
        
        chat = Chat(pairs, reflections)
        return chat.respond(sentence)

        # for word in sentence.split():
        #     if word.lower() in GREETING_INPUTS:
        #         return random.choice(GREETING_RESPONSES)
        
        
        

    def response(user_response):
        robo_response = ''

        TfidfVec = TfidfVectorizer(tokenizer=LemNormalize, stop_words='english')
        tfidf = TfidfVec.fit_transform(sent_tokens)
        vals = cosine_similarity(tfidf[-1], tfidf)
        idx = vals.argsort()[0][-2]
        flat = vals.flatten()
        flat.sort()
        req_tfidf = flat[-2]


        if (req_tfidf == 0):
            robo_response = robo_response + "I am sorry! I don't understand you"
            return robo_response
        else:
            robo_response = robo_response + sent_tokens[idx]
            return robo_response

    flag = True
    # print("ROBO: My name is Robo. I will answer your queries about Chatbots. If you want to exit, type Bye!")
    while (flag == True):
        user_response = _message
        user_response = user_response.lower()
        if (user_response != 'bye'):
            if (user_response == 'thanks' or user_response == 'thank you'):
                flag = False
                return("You are welcome..")
            else:
                if (greeting(user_response) != None):
                    flag = False
                    return(greeting(user_response))
                    
                if(user_response == 'offer report'):

                    url = "http://api.icubeswire.co/partner/get/performance"

                    payload={'pagination[page]': ' 1',
                    'pagination[perpage]': ' 10',
                    'sort[sort]': ' asc',
                    'sort[field]': ' campId',
                    'campId[]': '327',
                    'startDate': '2021-05-01',
                    'endDate': '2021-06-10'}
                    files=[

                    ]
                    headers = {
                    'apiKey': '29ef8aee81889ca4297779247d6cd6ffbbc62e41',
                    }

                    response = requests.request("POST", url, headers=headers, data=payload, files=files)

                    res = (response.json())['data'][0]
                    flag = False
                    return (str(res['conversions']))
                    
                    
                    
                    
                else:
                    sent_tokens.append(user_response)
                    word_tokens = word_tokens + nltk.word_tokenize(user_response)
                    final_words = list(set(word_tokens))
                    flag = False
                    return(response(user_response))
                    sent_tokens.remove(user_response)
        else:
            flag = False
            return("ROBO: Bye! take care..")

if __name__ == "__main__":
    chatbot()

