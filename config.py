from pydantic import BaseSettings

class Settings(BaseSettings):
    app_name: str = ""
    aws_access_key_id: str = ""
    aws_secret_access_key = ""
    aws_default_region = ""
    class Config:
        env_file = '.env'
        env_file_encoding = 'utf-8'